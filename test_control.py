import time
from control.peltier_controller import PeltierController


class FakeBridge():
    def __init__(self, INA="P2_33", INB="P2_3", PWM="P1_36", FREQ=500):
        pass

    def turn_off_bridge(self):
        print("H Bridge OFF")

    def set_bridge(self, duty, side):
        print("Configurando peltier en {} {}".format(duty, side))


class FakeTempSensor():
    def __init__(self):
        pass

    def get_temperature(self):
        return float(input("Temperatura leida por el sensor:"))


def test_control():
    bridge = FakeBridge()
    temp_sensor = FakeTempSensor()
    peltier = PeltierController(bridge, temp_sensor)
    while True:
        temp_to_go = float(input("Temperatura a alcanzar:"))
        peltier.run_control(temp_to_go)
        time.sleep(1)


test_control()
