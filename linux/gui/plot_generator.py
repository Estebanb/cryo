import matplotlib.pyplot as plt


def generate_plot(points_to_follow, points_made, plot_path='/tmp/cryo/curva_actual.png', dpi=50):
    plt.gcf().clear()
    SMALL_SIZE = 8
    MEDIUM_SIZE = 10
    BIGGER_SIZE = 18
    plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=15)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.ylabel('Temperature(in celcius)')
    plt.title("Curva")
    plt.xlabel('Time(in seconds)')
    plt.plot(points_made, label='Temp Actual', linewidth=4)
    if points_to_follow:
        plt.plot(points_to_follow, label='Temp Buscada')
    plt.legend()
    plt.savefig(plot_path, dpi=dpi)

