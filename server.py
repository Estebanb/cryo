import socket
import json
import datetime
from Adafruit_BBIO.SPI import SPI
from temp_sensor.adafruit_max31865 import MAX31865
from h_bridge.h_bridge_driver import HBridge
from control.peltier_controller import PeltierController


def configurate_spi_for_max31865():
    spi = SPI(2, 1)
    spi.msh = 500000
    spi.cshigh = False
    spi.mode = 3
    return spi


class ControlServerSock():
    def __init__(self, host='127.0.0.1', port=5001):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.spi = configurate_spi_for_max31865()
        self.bridge = HBridge()
        self.temp_sensor = MAX31865(self.spi)
        self.peltier = PeltierController(self.bridge, self.temp_sensor)
        date_t = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        self.logger_file_name = "reports/"+date_t+'temperatures.txt'

    def run_control(self, temp_to_go):

        temp_actual, output, duty, bridge_side, bridge_status = self.peltier.run_control(temp_to_go)

        f_log = open(self.logger_file_name, 'a')
        f_log.write(str(temp_actual)+','+str(output)+','+str(duty)+','+str(bridge_side)+','+str(temp_actual)+','+str(bridge_status)+'\n')
        f_log.close()
        current_status = {}
        current_status['temp_actual'] = temp_actual
        current_status['temp_to_go'] = temp_to_go
        current_status['output'] = output
        current_status['duty'] = duty
        current_status['bridge_side'] = bridge_side
        current_status['bridge_status'] = bridge_status
        return current_status

    def disconnect(self):
        self.sock.close()

    def run(self):
        self.sock.bind((self.host, self.port))
        self.sock.listen(1)

        while True:
            print("Waiting for new connection")
            conn, addr = self.sock.accept()
            print("Connection from: " + str(addr))
            print("Run control")
            conn.settimeout(1)
            temp_to_go = 25

            while True:
                """ Este loop recive mensajes socket, hay un timeout de 1 segundo
                    si en 1 segundo no entra en mensaje, se actualiza el control 
                    con la última temperatura solicitada
                """
                print("Running control ", datetime.datetime.now())
                control_status = self.run_control(temp_to_go)
                try:
                    data = conn.recv(1024).decode()
                    if not data:
                            break
                    print ("from connected  user: " + str(data))
                    msg = json.loads(str(data))

                    if 'temp_to_go' in msg:
                        temp_to_go = msg['temp_to_go']

                    conn.send(json.dumps(control_status).encode())
                except socket.timeout:
                    pass
        self.disconnect()


def test():
    control = ControlServerSock()
    control.run()


if __name__ == '__main__':
    test()
