from tkinter import Label, Frame, Button, CENTER, LEFT, DISABLED, NORMAL, Tk, font
from menu_common import MenuBase
from menu_curvas import MenuEmbriones, MenuEsperma


class MenuCryo(MenuBase):                     
    def __init__(self, parent, controller):
        MenuBase.__init__(self, parent, controller)
        self.controller = controller
        self.make_widgets()
        self.all_focuses = [self.bt_embriones, self.bt_esperma]

    def make_buttons(self):
        self.bt_embriones = Button(self, highlightcolor="red", highlightthickness=4, text='Embriones', command=lambda: self.controller.show_frame(MenuEmbriones), width=13, font=self.fuente_button)
        self.bt_esperma = Button(self, highlightcolor="red", highlightthickness=4, text='Espermatozoides', command=lambda: self.controller.show_frame(MenuEsperma), width=13, font=self.fuente_button)
        self.bt_embriones.grid(row=3, column=0)
        self.bt_esperma.grid(row=4, column=0)

    def make_widgets(self):             
        self.make_label_title()
        self.make_buttons()
        self.grid_columnconfigure(1, minsize=100)
        self.grid_rowconfigure(5, minsize=60)
        self.grid_rowconfigure(2, minsize=20)
        self.grid_rowconfigure(1, minsize=20)

    def postupdate(self):
        self.bt_embriones.focus_set()
        self.actual_focus = self.all_focuses.index(self.bt_embriones)
        self.bind_class("Button", "<Down>", lambda event: self.move_focus(+1))
        self.bind_class("Button", "<Up>", lambda event: self.move_focus(-1))

    def make_label_title(self):
        label_temperature = Label(self, text="Menu cryo", font=self.fuente_label_title)
        label_temperature.grid(row=0, column=0)

