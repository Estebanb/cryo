import csv
from datetime import datetime, timedelta
from tkinter import Label, Frame, Button, CENTER, LEFT, DISABLED, NORMAL, Tk, font, messagebox
from PIL import Image, ImageTk
from menu_common import MenuBase
from plot_generator import generate_plot
from buzzer import Buzzer
import time


class ScreenGraph(MenuBase):
    def __init__(self, parent, controller):
        MenuBase.__init__(self, parent, controller)
        self.controller = controller
        self.make_widgets()
        self.all_focuses = [self.label_img]
        self.seeding = False
        self.buzzer = Buzzer()
        self.output_error_acumulator = 0 
        self.ask_for_n2 = True

    def make_label_graph(self, update=False):
        if update:
            image = Image.open('/tmp/cryo/curva_actual.png')
            photo = ImageTk.PhotoImage(image, master=self)
            self.label_img = Label(self, image=photo)
            self.label_img.image = photo
        else:
            self.label_img = Label(self)
        self.label_img.grid(row=0, column=0)

    def make_widgets(self):
        self.make_label_graph()
        #self.grid_columnconfigure(1, minsize=100)
        #self.grid_rowconfigure(5, minsize=60)
        #self.grid_rowconfigure(2, minsize=20)
        #self.grid_rowconfigure(1, minsize=20)

    def postupdate(self, curva_path):
        self.label_img.focus_set()
        self.actual_focus = 0
        self.bind_class("Button", "<Down>", lambda event: self.move_focus(+1))
        self.bind_class("Button", "<Up>", lambda event: self.move_focus(-1))

        if "embrion" in curva_path:
            print("Curva de embriones detectada")
            self.seeding = True

        self.get_points_from_curva(curva_path)
        self.keeping_updating_graph = True
        self.points_made = []
        self.update_graph()

    def update_graph(self):
        if self.keeping_updating_graph:
            self.buzzer.configure()
            #self.update_points_made()
            if self.seeding and self.seeding_point in self.points_made:
                self.buzzer.turn_on()
                messagebox.showinfo('Seeding', "Haga el seeding y oprima OK")
                self.buzzer.turn_off()
                self.seeding = False

            generate_plot(self.points_to_follow, self.points_made)
            if (len(self.points_to_follow) - 1) <= len(self.points_made):
                self.finish()
            control_status = self.controller.control.set_temperature(self.points_to_follow[len(self.points_made)])
            self.points_made.append(control_status['temp_actual'])

            if (int(control_status['temp_actual']) - int(control_status['temp_to_go'])) > 0.75:
                self.output_error_acumulator = self.output_error_acumulator + 1 
            else:
                self.output_error_acumulator = 0 

            if self.output_error_acumulator > 10 and self.ask_for_n2:
                self.buzzer.turn_on()
                messagebox.showwarning('Se requiere mas nitrogeno', "Recargue nitrogeno")
                self.buzzer.turn_off()
                self.output_error_acumulator = 0 
                self.ask_for_n2 = False

            self.make_label_graph(True)
            self.after(1000, self.update_graph)

    def finish(self):
        self.buzzer.configure()
        self.buzzer.turn_on()
        time.sleep(0.5)
        self.buzzer.turn_off()
        self.keeping_updating_graph = False
        messagebox.showinfo('Criogenización finalizada', "Criogenizacion realizada\ncon exito quite las pajuelas\n y oprima OK")
        self.controller.show_principal()


    def update_points_made(self):
        if len(self.points_to_follow) <= len(self.points_made) -1:
            self.buzzer.configure()
            self.buzzer.turn_on()
            time.sleep(2)
            self.buzzer.turn_off()
            self.keeping_updating_graph = False
            messagebox.showinfo('Criogenización finalizada', "Criogenizacion realizada con exito, quite las pajuelas y oprima OK")
            self.controller.show_principal()

    def get_points_from_curva(self, file_path="curva.csv"):
        self.points_to_follow = []
        with open(file_path) as csvfile:
            for idx, row in enumerate(csv.reader(csvfile)):
                if idx == 0:
                    print(row)
                elif idx == 1 and self.seeding:
                    self.seeding_point = float(row[0])
                else:
                    self.points_to_follow.append(float(row[0]))

