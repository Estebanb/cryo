from tkinter import Label, Frame, Button, CENTER, LEFT, DISABLED, NORMAL, Tk, font
from menu_common import MenuBase


class MenuConfig(MenuBase):                     
    def __init__(self, parent, controller):
        MenuBase.__init__(self, parent, controller)
        self.controller = controller
        self.make_widgets()
        self.all_focuses = [self.bt_exportar_curvas]

    def make_buttons(self):
        self.bt_exportar_curvas = Button(self, highlightcolor="red", highlightthickness=4, text='Exportar Curvas', command=self.quit, width=13, font=self.fuente_button)
        self.bt_exportar_curvas.grid(row=3, column=0)

    def make_widgets(self):
        self.make_label_title()
        self.make_buttons()
        self.grid_columnconfigure(1, minsize=100)
        self.grid_rowconfigure(5, minsize=60)
        self.grid_rowconfigure(2, minsize=20)
        self.grid_rowconfigure(1, minsize=20)

    def postupdate(self):
        self.bt_exportar_curvas.focus_set()
        self.actual_focus = 0
        self.bind_class("Button", "<Down>", lambda event: self.move_focus(+1))
        self.bind_class("Button", "<Up>", lambda event: self.move_focus(-1))

    def make_label_title(self):
        label_temperature = Label(self, text="Configuracion", font=self.fuente_label_title)
        label_temperature.grid(row=0, column=0)
