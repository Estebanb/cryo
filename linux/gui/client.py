import socket
import time
import json


class ControlClientSock():
    def __init__(self, host='127.0.0.1', port=5001):
        self.host = host
        self.port = port
        self.sock = socket.socket()

    def connect(self):
        self.sock.connect((self.host, self.port))

    def disconnect(self):
        self.sock.close()

    def get_temperature(self):
        self.sock.send(json.dumps({"get_temp": None}).encode())
        control_status_raw = self.sock.recv(1024).decode()
        control_status = json.loads(control_status_raw)
        return control_status['temp_actual']

    def set_temperature(self, temperature):
        self.sock.send(json.dumps({"temp_to_go": temperature}).encode())
        control_status_raw = self.sock.recv(1024).decode()
        print("raw", control_status_raw)
        self.control_status = json.loads(control_status_raw)
        print('Received from server:')
        print(self.control_status)
        return self.control_status


def test_sock():
    control = ControlClientSock()
    control.connect()
    while True:
        time.sleep(2)
        control.set_temperature(30)


if __name__ == '__main__':
    test_sock()
