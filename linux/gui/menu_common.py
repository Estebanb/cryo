import time
from tkinter import Label, Frame, Button, CENTER, LEFT, DISABLED, NORMAL, Tk, font


def wrapper(e):
    e.widget.invoke()


class MenuBase(Frame):
    def __init__(self, parent, controller):    
        Frame.__init__(self, parent)
        self.controller = controller
        self.make_label_date_time()
        self.update_date_time()
        self.actual_focus = 0
        self.bind_class("Button", "<Return>", wrapper)
        self.fuente_button = font.Font(family='Helvetica', size=12, weight='bold')
        self.fuente_label = font.Font(family='Helvetica', size=12)
        self.fuente_label_title = font.Font(family='Helvetica', size=14, weight='bold')
        self.make_label_temperatura()
        self.update_label_temperatura()
        self.bind_all("<Escape>", lambda event: self.controller.show_principal(self))

    def make_label_temperatura(self):
        self.label_temperature = Label(self, text="25 °C", font=self.fuente_label)
        self.label_temperature.grid(row=0, column=3)

    def make_label_date_time(self):
        self.label_date_time = Label(self)
        self.label_date_time.grid(row=8, column=3)

    def update_date_time(self):
        time_now = time.strftime('%H:%M:%S')
        self.label_date_time.configure(text=time_now)
        self.after(1000, self.update_date_time)

    def update_label_temperatura(self):
        temp_now = self.controller.temp_now
        self.label_temperature.configure(text=temp_now)
        self.after(1000, self.update_label_temperatura)

    def move_focus(self, step):
        next_pos = self.actual_focus + step
        if next_pos >= len(self.all_focuses) or next_pos < 0:
            return
        self.actual_focus = next_pos
        self.all_focuses[self.actual_focus].focus_set()
 
