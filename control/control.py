import datetime
from Adafruit_BBIO.SPI import SPI
from temp_sensor.adafruit_max31865 import MAX31865
from h_bridge.h_bridge_driver import HBridge
from control.peltier_controller import PeltierController


def configurate_spi_for_max31865():
    spi = SPI(2, 1)
    spi.msh = 500000
    spi.cshigh = False
    spi.mode = 3
    return spi


def process_control(temp_to_go):
    spi = configurate_spi_for_max31865()
    bridge = HBridge()
    temp_sensor = MAX31865(spi)
    peltier = PeltierController(bridge, temp_sensor)

    date_t = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    logger_file_name = "reports/"+date_t+'temperatures.txt'

    temp_actual, output, duty, bridge_side, bridge_status = peltier.run_control(temp_to_go)

    f_log = open(logger_file_name, 'a')
    f_log.write(str(temp_actual)+','+str(output)+','+str(duty)+','+str(bridge_side)+','+str(temp_actual)+','+str(bridge_status)+'\n')
    f_log.close()
    current_status = {}
    current_status['temp_actual'] = temp_actual
    current_status['temp_to_go'] = temp_to_go
    current_status['output'] = output
    current_status['duty'] = duty
    current_status['bridge_side'] = bridge_side
    current_status['bridge_status'] = bridge_status
    return current_status
