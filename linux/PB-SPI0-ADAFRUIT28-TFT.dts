/dts-v1/;
/plugin/;

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/pinctrl/am33xx.h>
#include <dt-bindings/interrupt-controller/irq.h>

/ {
/*
 * Free up the pins used by the cape from the pinmux helpers.
*/ 
	fragment@0 {
		target = <&ocp>;
		__overlay__ {
			P1_06_pinmux { status = "disabled"; };	/* LCD - CS */
			P1_08_pinmux { status = "disabled"; };	/* LCD - SCLK */
			P1_10_pinmux { status = "disabled"; };	/* LCD - MOSI? */
			P1_12_pinmux { status = "disabled"; };	/* LCD - MISO? */
			P2_01_pinmux { status = "disabled"; };	/* LCD - LED */
			P2_17_pinmux { status = "disabled"; };	/* LCD - DC */
			P2_19_pinmux { status = "disabled"; };	/* LCD - RST */
            		P2_02_pinmux { status = "disabled"; }; /* keys - right */
            		P2_04_pinmux { status = "disabled"; }; /* keys - down */
            		P2_06_pinmux { status = "disabled"; }; /* keys - up */
            		P2_08_pinmux { status = "disabled"; }; /* keys - left */
            		P2_18_pinmux { status = "disabled"; }; /* keys - enter */
            		P2_22_pinmux { status = "disabled"; }; /* keys - esc */
		};
	};

	/*
	 * Free up the gpios used by the cape-universal gpio helpers.
	 */
	fragment@1 {
		target = <&ocp>;
		__overlay__ {
			cape-universal { status = "disabled"; };
		};
	};

	/*
	 * Make a new set of gpio helpers.
	 */
	fragment@2 {
		target = <&ocp>;
		__overlay__ {
			cape-universal@1 {
				compatible = "gpio-of-helper";
				status = "okay";
				pinctrl-names = "default";
				pinctrl-0 = <>;

				P1_02 {
					gpio-name = "P1_02";
					gpio = <&gpio2 23 0>;
					input;
					dir-changeable;
				};

				P1_04 {
					gpio-name = "P1_04";
					gpio = <&gpio2 25 0>;
					input;
					dir-changeable;
				};

				P1_20 {
					gpio-name = "P1_20";
					gpio = <&gpio0 20 0>;
					input;
					dir-changeable;
				};

				P1_26 {
					gpio-name = "P1_26";
					gpio = <&gpio0 12 0>;
					input;
					dir-changeable;
				};

				P1_28 {
					gpio-name = "P1_28";
					gpio = <&gpio0 13 0>;
					input;
					dir-changeable;
				};

				P1_29 {
					gpio-name = "P1_29";
					gpio = <&gpio3 21 0>;
					input;
					dir-changeable;
				};

				P1_30 {
					gpio-name = "P1_30";
					gpio = <&gpio1 11 0>;
					input;
					dir-changeable;
				};

				P1_31 {
					gpio-name = "P1_31";
					gpio = <&gpio3 18 0>;
					input;
					dir-changeable;
				};

				P1_32 {
					gpio-name = "P1_32";
					gpio = <&gpio1 10 0>;
					input;
					dir-changeable;
				};

				P1_33 {
					gpio-name = "P1_33";
					gpio = <&gpio3 15 0>;
					input;
					dir-changeable;
				};

				P1_34 {
					gpio-name = "P1_34";
					gpio = <&gpio0 26 0>;
					input;
					dir-changeable;
				};

				P1_35 {
					gpio-name = "P1_35";
					gpio = <&gpio2 24 0>;
					input;
					dir-changeable;
				};

				P1_36 {
					gpio-name = "P1_36";
					gpio = <&gpio3 14 0>;
					input;
					dir-changeable;
				};

				P2_03 {
					gpio-name = "P2_03";
					gpio = <&gpio0 23 0>;
					input;
					dir-changeable;
				};

				P2_05 {
					gpio-name = "P2_05";
					gpio = <&gpio0 30 0>;
					input;
					dir-changeable;
				};

				P2_07 {
					gpio-name = "P2_07";
					gpio = <&gpio0 31 0>;
					input;
					dir-changeable;
				};

				P2_09 {
					gpio-name = "P2_09";
					gpio = <&gpio0 15 0>;
					input;
					dir-changeable;
				};

				P2_10 {
					gpio-name = "P2_10";
					gpio = <&gpio1 20 0>;
					input;
					dir-changeable;
				};

				P2_11 {
					gpio-name = "P2_11";
					gpio = <&gpio0 14 0>;
					input;
					dir-changeable;
				};

				P2_20 {
					gpio-name = "P2_20";
					gpio = <&gpio2 0 0>;
					input;
					dir-changeable;
				};

				P2_24 {
					gpio-name = "P2_24";
					gpio = <&gpio1 12 0>;
					input;
					dir-changeable;
				};

				P2_25 {
					gpio-name = "P2_25";
					gpio = <&gpio1 9 0>;
					input;
					dir-changeable;
				};

				P2_27 {
					gpio-name = "P2_27";
					gpio = <&gpio1 8 0>;
					input;
					dir-changeable;
				};

				P2_28 {
					gpio-name = "P2_28";
					gpio = <&gpio3 20 0>;
					input;
					dir-changeable;
				};

				P2_29 {
					gpio-name = "P2_29";
					gpio = <&gpio0 7 0>;
					input;
					dir-changeable;
				};

				P2_30 {
					gpio-name = "P2_30";
					gpio = <&gpio3 17 0>;
					input;
					dir-changeable;
				};

				P2_31 {
					gpio-name = "P2_31";
					gpio = <&gpio0 19 0>;
					input;
					dir-changeable;
				};

				P2_32 {
					gpio-name = "P2_32";
					gpio = <&gpio3 16 0>;
					input;
					dir-changeable;
				};

				P2_33 {
					gpio-name = "P2_33";
					gpio = <&gpio1 13 0>;
					input;
					dir-changeable;
				};

				P2_34 {
					gpio-name = "P2_34";
					gpio = <&gpio3 19 0>;
					input;
					dir-changeable;
				};

				P2_35 {
					gpio-name = "P2_35";
					gpio = <&gpio2 22 0>;
					input;
					dir-changeable;
				};
			};
		};
	};

	fragment@3 {
		target = <&am33xx_pinmux>;
		__overlay__ {
			pwm_bl_pins: pinmux_pwm_bl_pins {
				pinctrl-single,pins = <
					0x48 0x06       /* ehrpwm1a, MODE6 */
				>;
			};
		};
	};

	fragment@4 {
		target = <&epwmss1>;
		__overlay__ {
			status = "okay";
		};
	};

	fragment@5 {
		target = <&ehrpwm1>;
		__overlay__ {
			status = "okay";
		};
	};

	fragment@6 {
		target = <&ocp>;
		__overlay__ {
			backlight {
				compatible = "pwm-backlight";
				pinctrl-names = "default";
				pinctrl-0 = <&pwm_bl_pins>;
				status = "okay";

				pwms = <&ehrpwm0 0 500000 0>;
				pwm-names = "backlight";
				brightness-levels = <0 4 8 16 32 64 128 255>;
				default-brightness-level = <7>; /* index to the array above */
			};
		};
	};

	fragment@7 {
		target = <&spi0>;
		__overlay__ {
			#address-cells = <1>;
			#size-cells = <0>;
			status = "okay";

			channel@0 {
				status = "disabled";
			};
			channel@1 {
				status = "disabled";
			};

			adafruit28: adafruit28@0{
				compatible = "ilitek,ili9341";
				pinctrl-names = "default";
				pinctrl-0 = <
					&P1_06_spi_cs_pin	/* CS */
					&P1_08_spi_sclk_pin	/* SCLK */
					&P1_10_spi_pin		/* D0 (MOSI?) */
					&P1_12_spi_pin		/* D1 (MISO?) */
					&P2_19_gpio_pin		/* RST */
					&P2_17_gpio_pin		/* DC */
					/* &P2_01_gpio_pin		/\* LED *\/ */
				>;
				reg = <0>;
				buswidth = <8>;
				reset-gpios = <&gpio0 27 0>; /* P2_17 - 27 */
				dc-gpios = <&gpio2 1 0>; /* P2_17 - 65 */
				/* led-gpios = <&gpio1 18 1>; /\* P2_01 - PWM1A *\/ */
				debug = <1>;
				spi-max-frequency = <3000000>;
				rotate = <90>;
				bgr;
				fps = <30>;
			};
		};
	};
    /*
	 * Load the drivers for the buttons and LEDs.
	 */
	fragment@8 {
		target-path="/";
		__overlay__ {

			gpio-keys {
				compatible = "gpio-keys";
				autorepeat;

				pinctrl-names = "default";
				pinctrl-0 = <
					&P2_02_gpio_pu_pin	/* down */
					&P2_04_gpio_pu_pin	/* down */
					&P2_06_gpio_pu_pin	/* up */
					&P2_08_gpio_pu_pin	/* left */
					&P2_18_gpio_pu_pin	/* enter */
					&P2_22_gpio_pu_pin	/* esc */
				>;
		
				down {
					label = "down";
					linux,code = <108>;
					gpios = <&gpio1 28 GPIO_ACTIVE_LOW>;
				};
		
				left {
					label = "left";
					linux,code = <105>;
					gpios = <&gpio1 26 GPIO_ACTIVE_LOW>;
				};
		
				up {
					label = "up";
					linux,code = <103>;
					gpios = <&gpio1 25 GPIO_ACTIVE_LOW>;
				};
		
				right {
					label = "right";
					linux,code = <106>;
					gpios = <&gpio1 27 GPIO_ACTIVE_LOW>;
				};
	
				esc {
					label = "esc";
					linux,code = <1>;
					gpios = <&gpio1 14 GPIO_ACTIVE_LOW>;
				};
		
				enter {
					label = "enter";
					linux,code = <28>;
					gpios = <&gpio1 15 GPIO_ACTIVE_LOW>;
				};
			};
		};
	};
};
