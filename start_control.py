import time
import datetime
from Adafruit_BBIO.SPI import SPI
from temp_sensor.adafruit_max31865 import MAX31865
from h_bridge.h_bridge_driver import HBridge
from control.peltier_controller import PeltierController


def configurate_spi_for_max31865():
    spi = SPI(2, 1)
    spi.msh = 500000
    spi.cshigh = False
    spi.mode = 3
    return spi

tiempo=[25]

for i in range(0, 32*60):
    tiempo.append(tiempo[-1] - 1/60)

for i in range(0, 2*60):
    tiempo.append(tiempo[-1])

for i in range(0, 56*60):
    tiempo.append(tiempo[-1] - 0.5/60)


def run_control():
    spi = configurate_spi_for_max31865()
    bridge = HBridge()
    temp_sensor = MAX31865(spi)
    peltier = PeltierController(bridge, temp_sensor)
    temp_to_go = float(input("Temperatura a alcanzar:"))
    date_t = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    file_name = "reports/"+date_t+'temperatures.txt'
    print("Nombre del reporte: {}".format(file_name))
    contador = 0
    try:
        while True:
            #Leer de un file la temp
            temp_actual, output, duty, bridge_side = peltier.run_control(tiempo[contador])
            print("                                    Temperatura a alcanzar: {}, Temperatura actual: {}, Error actual: {}".format(tiempo[contador], temp_actual,tiempo[contador]- temp_actual))
            time.sleep(1)
            f = open(file_name, 'a')
            f.write(str(temp_actual)+','+str(output)+','+str(duty)+','+str(bridge_side)+','+str(tiempo[contador])+'\n')
            f.close()
            contador+=1
    except KeyboardInterrupt:
        bridge.turn_off_bridge()


