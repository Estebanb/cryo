import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import subprocess
import time

def graf_lines_with_point(point):
	plt.figure(figsize=(3.2,2.4))
	plt.xlabel('Temperature')
	plt.ylabel('Time')
	plt.plot([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], [25,25,25,25, 20, 15, 10, 5, 0,0,0, -5, -10,-10,-10, -15, -20, -25,-25,-25, -30])
	plt.plot(point[0], point[1], 'ro')
	plt.savefig('/tmp/actual_graph.png')
	plt.close()
	subprocess.check_call(['fbi -T 2 --noverbose /tmp/actual_graph.png'], shell=True)

points = [[0,25],[1,25],[2,25],[3,25],[4,20],[5,15],[6,10],[7,5],[8,0],[9,0],[10,0],[11,-5],[12,-10],[13,-10],[14,-10],[15,-15],[16,-20],[17,-25],[18,-25],[19,-25],[20,-30]]

for point in points:
	graf_lines_with_point(point)
	time.sleep(1)


