from tkinter import Label, Button, Entry, IntVar, END
from menu_common import MenuBase
from plot_generator import generate_plot
from screen_vista_previa_graph import ScreenVistaPrevia


def cambiar_temp_inicial(self, cambio):
    self.temp_inicial += cambio
    self.e_temp_inicial.delete(0, END)
    self.e_temp_inicial.insert(0, self.temp_inicial)


def cambiar_pendiente(self, cambio):
    self.pendiente += cambio
    self.e_pendiente.delete(0, END)
    self.e_pendiente.insert(0, self.pendiente)


def cambiar_seeding(self, cambio):
    self.temp_seeding += cambio
    self.e_seeding.delete(0, END)
    self.e_seeding.insert(0, self.temp_seeding)


def cambiar_temp_final(self, cambio):
    self.temp_final += cambio
    self.e_temp_final.delete(0, END)
    self.e_temp_final.insert(0, self.temp_final)


def generate_csv(path, curva_name, curva, seeding=None):
    """
    La primera linea del csv es el nombre de la curva, la segunda el seeding y las posteriores las temperaturas
    """
    f = open(path, 'w')
    f.write('{}\n'.format(curva_name))
    if seeding:
        f.write('{}\n'.format(seeding))
    for value in curva:
        f.write(str(value)+'\n')
    f.close()


class MenuCurvasEditor(MenuBase):
    def __init__(self, parent, controller):
        MenuBase.__init__(self, parent, controller)
        self.controller = controller
        self.temp_inicial = 25
        self.pendiente = -0.5
        self.temp_seeding = -7
        self.temp_final = -35
        self.make_widgets()
        self.all_focuses = [self.e_temp_inicial, self.e_pendiente, self.e_seeding, self.e_temp_final, self.bt_save]
        # Agregar entry para tiempo de seeding y pendiente desupes de seeding
        # Agregar un entry para seleccionar entre ovulo y espermatozoide

    def generate_curva(self):
        print(self.e_temp_final.get(), self.e_pendiente.get(), self.e_seeding.get(), self.e_temp_inicial.get())
        curva = [float(self.e_temp_inicial.get())]
        tiempo_hasta_seeding = int((-float(self.e_temp_inicial.get())+float(self.e_seeding.get()))/float(self.e_pendiente.get()))
        tiempo_hast_t_final = int((float(self.e_temp_final.get()) - float(self.e_seeding.get()))/float(self.e_pendiente.get()))

        for i in range(0, tiempo_hasta_seeding*60):
            curva.append(curva[-1] + float(self.e_pendiente.get())/60)
        for i in range(0, 5*60):
            # Duracion seeding hardcodeada a 5 minutos
            curva.append(curva[-1])
        for i in range(0, tiempo_hast_t_final*60):
            curva.append(curva[-1] + float(self.e_pendiente.get())/60)

        generate_plot(None, curva, '/tmp/cryo/curva_temporal.png', dpi=41)
        generate_csv('/tmp/cryo/curva_temporal.csv', 'curva_custom', curva, self.e_seeding.get())
        self.controller.show_frame(ScreenVistaPrevia)

    def make_buttons(self):
        self.bt_save = Button(self, highlightcolor="red", highlightthickness=4, text='Save', command=self.generate_curva, width=13, font=self.fuente_button)
        self.bt_save.grid(row=8, column=0)

    def make_label_title(self):
        label_temperature = Label(self, text="Editor de Curvas", font=self.fuente_label_title)
        label_temperature.grid(row=0, column=0)

    def make_labels_entrys(self):
        label_temp_inicial = Label(self, text="Temp. inicial", font=self.fuente_label)
        label_temp_inicial.grid(row=3, column=0)
        label_pendiente = Label(self, text="Pendiente(ºC/min)", font=self.fuente_label)
        label_pendiente.grid(row=4, column=0)
        label_temp_seeding = Label(self, text="Temp. seeding", font=self.fuente_label)
        label_temp_seeding.grid(row=5, column=0)
        label_temp_final = Label(self, text="Temp final(ºC)", font=self.fuente_label)
        label_temp_final.grid(row=6, column=0)

    def make_entry_temp_inicial(self):
        self.e_temp_inicial = Entry(self)
        self.e_temp_inicial.config(width=5)
        self.e_temp_inicial.insert(0, self.temp_inicial)
        self.e_temp_inicial.grid(row=3, column=1)
        self.e_temp_inicial.bind("<Down>", lambda event: cambiar_temp_inicial(self, -1))
        self.e_temp_inicial.bind("<Up>", lambda event: cambiar_temp_inicial(self, +1))

    def make_entry_pendiente(self):
        self.e_pendiente = Entry(self)
        self.e_pendiente.config(width=5)
        self.e_pendiente.insert(0, self.pendiente)
        self.e_pendiente.grid(row=4, column=1)
        self.e_pendiente.bind("<Down>", lambda event: cambiar_pendiente(self, -0.1))
        self.e_pendiente.bind("<Up>", lambda event: cambiar_pendiente(self, +0.1))

    def make_entry_seeding(self):
        self.e_seeding = Entry(self)
        self.e_seeding.config(width=5)
        self.e_seeding.insert(0, self.temp_seeding)
        self.e_seeding.grid(row=5, column=1)
        self.e_seeding.bind("<Down>", lambda event: cambiar_seeding(self, -0.5))
        self.e_seeding.bind("<Up>", lambda event: cambiar_seeding(self, +0.5))

    def make_entry_temp_final(self):
        self.e_temp_final = Entry(self)
        self.e_temp_final.config(width=5)
        self.e_temp_final.insert(0, self.temp_final)
        self.e_temp_final.grid(row=6, column=1)
        self.e_temp_final.bind("<Down>", lambda event: cambiar_temp_final(self, -0.5))
        self.e_temp_final.bind("<Up>", lambda event: cambiar_temp_final(self, +0.5))

    def make_widgets(self):
        self.make_label_title()
        self.make_entry_temp_inicial()
        self.make_entry_pendiente()
        self.make_entry_seeding()
        self.make_entry_temp_final()
        self.make_buttons()
        self.make_labels_entrys()
        self.grid_columnconfigure(1, minsize=100)
        self.grid_rowconfigure(7, minsize=40)
        self.grid_rowconfigure(1, minsize=20)

    def postupdate(self):
        self.e_temp_inicial.focus_set()
        self.bind_class("Button", "<Down>", self.move_focus(+1))
        self.bind_class("Button", "<Up>", self.move_focus(-1))
        self.e_temp_inicial.bind("<Return>", lambda event: self.move_focus(+1))
        self.e_pendiente.bind("<Return>", lambda event: self.move_focus(+1))
        self.e_seeding.bind("<Return>", lambda event: self.move_focus(+1))
        self.e_temp_final.bind("<Return>", lambda event: self.move_focus(+1))

