import os
import time
from tkinter import Listbox, Label, messagebox, StringVar
from menu_common import MenuBase
from screen_wait import ScreenWait


def onselect(evt, self):
    w = evt.widget
    index = int(w.curselection()[0])
    value = w.get(index)
    curva_path = self.curvas_base_path+value
    print('You selected item %d: "%s"' % (index, value))
    messagebox.showinfo('Nitrogeno', "Coloque nitrogeno en la\nmaquina y oprima OK")
    self.controller.show_frame(ScreenWait, curva_path=curva_path)


class MenuCurvas(MenuBase):                   
    def __init__(self, parent, controller, curvas_base_path):
        MenuBase.__init__(self, parent, controller)
        self.curvas_base_path = curvas_base_path
        self.make_widgets()
        self.all_focuses = [self.Lb_curvas_embriones]

    def make_listbox(self):
        curvas = os.listdir(self.curvas_base_path)
        if not curvas:
            curvas = ["No exiten curvas"]
        self.Lb_curvas_embriones = Listbox(self)
        for idx, curva in enumerate(curvas):
            self.Lb_curvas_embriones.insert(idx, curva)
        self.Lb_curvas_embriones.bind('<Return>', lambda event: onselect(event, self))
        self.Lb_curvas_embriones.focus_set()
        self.Lb_curvas_embriones.grid(row=1, column=0)

    def make_widgets(self):
        self.make_label_title()
        self.make_listbox()
        self.grid_columnconfigure(0, minsize=10)
        self.grid_columnconfigure(1, minsize=65)
        self.grid_rowconfigure(1, minsize=20)

    def postupdate(self):
        self.Lb_curvas_embriones.focus_set()
        self.make_listbox()
        #self.bind_class("Button", "<Down>", self.move_focus(+1))
        #self.bind_class("Button", "<Up>", self.move_focus(-1))


class MenuEmbriones(MenuCurvas):
    def __init__(self, parent, controller):
        MenuCurvas.__init__(self, parent, controller, curvas_base_path="curvas/embrion/")

    def make_label_title(self):
        label_temperature = Label(self, text="Curvas Embriones", font=self.fuente_label_title)
        label_temperature.grid(row=0, column=0)


class MenuEsperma(MenuCurvas):
    def __init__(self, parent, controller):
        MenuCurvas.__init__(self, parent, controller, curvas_base_path="curvas/espermatozoide/")

    def make_label_title(self):
        label_temperature = Label(self, text="Curvas Esperma", font=self.fuente_label_title)
        label_temperature.grid(row=0, column=0)
