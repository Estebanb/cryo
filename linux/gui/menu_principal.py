from tkinter import Button, Label
from menu_common import MenuBase
from menu_cryo import MenuCryo
from menu_config import MenuConfig
from menu_curvas_editor import MenuCurvasEditor


class MenuPrincipal(MenuBase):
    def __init__(self, parent, controller):
        MenuBase.__init__(self, parent, controller)
        self.controller = controller
        self.make_widgets()
        self.pack(side="top", fill="both", expand=True)
        self.all_focuses = [self.bt_cryogenizar, self.bt_edit_curvas, self.bt_config]

    def make_buttons(self):
        self.bt_cryogenizar = Button(self, highlightcolor="red", highlightthickness=4, text='Cryogenizar', command=lambda: self.controller.show_frame(MenuCryo), width=13, font=self.fuente_button)
        self.bt_edit_curvas = Button(self, highlightcolor="red", highlightthickness=4, text='Editar Curvas', command=lambda: self.controller.show_frame(MenuCurvasEditor), width=13, font=self.fuente_button)
        self.bt_config = Button(self, highlightcolor="red", highlightthickness=4, text='Configuracion', command=lambda: self.controller.show_frame(MenuConfig), width=13, font=self.fuente_button)
        self.bt_cryogenizar.grid(row=2, column=0)
        self.bt_edit_curvas.grid(row=3, column=0)
        self.bt_config.grid(row=4, column=0)

    def make_widgets(self):
        self.make_label_title()
        self.make_buttons()
        self.grid_columnconfigure(1, minsize=100)
        self.grid_rowconfigure(5, minsize=40)
        self.grid_rowconfigure(1, minsize=20)

    def postupdate(self):
        self.actual_focus = 0
        self.all_focuses[self.actual_focus].focus_set()
        self.bind_class("Button", "<Down>", lambda event: self.move_focus(+1))
        self.bind_class("Button", "<Up>", lambda event: self.move_focus(-1))

    def make_label_title(self):
        label_temperature = Label(self, text="Menu Principal", font=self.fuente_label_title)
        label_temperature.grid(row=0, column=0)

