import time
from datetime import datetime, timedelta
from tkinter import Label, Frame, Button, CENTER, LEFT, DISABLED, NORMAL, Tk, font, messagebox
from menu_common import MenuBase
from screen_graph import ScreenGraph


class ScreenWait(MenuBase):                     
    def __init__(self, parent, controller):
        MenuBase.__init__(self, parent, controller)
        self.controller = controller
        self.make_widgets()
        self.all_focuses = [self.label_wait]
        self.time_to_wait = 5 #5*60

    def make_label_wait(self):
        self.label_wait = Label(self, text="Espere que se estabilice el sistema", font=self.fuente_label_title)
        self.label_wait.grid(row=1, column=0)
        self.label_time_left = Label(self)
        self.label_time_left.grid(row=2, column=0)

    def make_widgets(self):
        self.make_label_wait()
        self.make_label_title()
        self.grid_columnconfigure(1, minsize=100)
        self.grid_rowconfigure(5, minsize=60)
        self.grid_rowconfigure(2, minsize=20)
        self.grid_rowconfigure(1, minsize=20)

    def postupdate(self, curva_path):
        self.label_wait.focus_set()
        self.curva_path = curva_path
        self.actual_focus = 0
        self.bind_class("Button", "<Down>", lambda event: self.move_focus(+1))
        self.bind_class("Button", "<Up>", lambda event: self.move_focus(-1))

        self.time_start = time.time()
        self.wait_for_system_stabilization()

    def make_label_title(self):
        label_temperature = Label(self, text="", font=self.fuente_label_title)
        label_temperature.grid(row=0, column=0)

    def wait_for_system_stabilization(self):
        if time.time() - self.time_start < self.time_to_wait:
            self.controller.control.set_temperature(25)
            delta=time.time() -self.time_start
            time_left = int(self.time_to_wait - delta) 
            date_time_left = timedelta(seconds=time_left)
            self.label_time_left.configure(text=date_time_left)
            self.after(1000, self.wait_for_system_stabilization)
        else:
            messagebox.showinfo('Pajuelas', "Coloque las pajuelas y oprima\n OK")
            self.controller.show_frame(ScreenGraph, curva_path=self.curva_path)
