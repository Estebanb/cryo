import tkinter as tk
from menu_cryo import MenuCryo
from menu_principal import MenuPrincipal
from menu_curvas import MenuEmbriones, MenuEsperma
from menu_config import MenuConfig
from menu_curvas_editor import MenuCurvasEditor
from screen_wait import ScreenWait
from screen_graph import ScreenGraph
from screen_vista_previa_graph import ScreenVistaPrevia
from client import ControlClientSock


class PantallaPrincipal(tk.Tk):
    def __init__(self, *args, **kwargs):
        self.initialize_control_client()
        self.temp_now = 0
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (MenuPrincipal, MenuCryo, MenuEmbriones, MenuEsperma, MenuConfig, MenuCurvasEditor, ScreenWait, ScreenGraph, ScreenVistaPrevia):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(MenuPrincipal)
        self.update_temp_from_server()

    def show_frame(self, cont, **kwargs):
        frame = self.frames[cont]
        frame.tkraise()
        if 'curva_path' in kwargs:
            frame.postupdate(kwargs['curva_path'])
        else:
            frame.postupdate()

    def show_principal(self, event=None):
        frame = self.frames[MenuPrincipal]
        frame.tkraise()
        frame.postupdate()

    def initialize_control_client(self):
        self.control = ControlClientSock()
        self.control.connect()

    def update_temp_from_server(self):
        self.temp_now = str(self.control.get_temperature())[:5]
        print(self.temp_now)
        self.after(5000, self.update_temp_from_server)



app = PantallaPrincipal()
app.geometry("320x240")
# app.attributes("-fullscreen", True)
app.mainloop()
