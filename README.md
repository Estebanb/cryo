# Graficar curvas de temperatura
Las curvas de temperatura son generadas a partir de "reportes" generados por los script de control.
Para poder graficar una curva es necesario haber previamente obtenido los datos.

He aquí un ejemplo de utilización del script diseñado para tal fin:

```python3 plot/plotter.py reports/20180727-043520temperatures.txt --graph_name "Intento de subir la temperatura desde -38 a 25ºC"```
