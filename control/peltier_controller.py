from .ivPID.PID import PID


class PeltierController():
    def __init__(self, bridge, temp_sensor, P=14, I=0.03, D=0.001, delta=0.5, DUTY_SCALE=10):
        self.bridge = bridge
        self.sensor = temp_sensor
        self.delta = delta
        self.duty_scale = DUTY_SCALE
        self.pid = PID(P, I, D)
        self.pid.setSampleTime(1)

    def _calculate_duty(self, output):
        if abs(output)*self.duty_scale > 100:
            return 100
        else:
            return int(abs(output)*self.duty_scale)

    def run_control(self, ideal_point):
        bridge_side = "None"
        bridge_status = "OFF"
        temp_actual = self.sensor.get_temperature()
        self.pid.update(temp_actual, ideal_point)
        output = self.pid.output
        print("La temperatura es: {}".format(temp_actual))
        print("El output es: {}".format(output))

        duty = self._calculate_duty(output)

        # Si esta adentro del margen de error delta*2, apagamos la peltier
        if output < self.delta and output > -self.delta:
            self.bridge.turn_off_bridge()

        # Si el error output es mayor al delta, enfriamos el lado A.
        elif output > self.delta:
            bridge_side = "B"
            bridge_status = "ON"
            self.bridge.set_bridge(duty, bridge_side)

        # Si el error output es menor al delta, calentamos el lado A.
        elif output < self.delta:
            bridge_side = "A"
            self.bridge.turn_off_bridge()
            #self.bridge.set_bridge(duty, bridge_side)

        return temp_actual, output, duty, bridge_side, bridge_status
