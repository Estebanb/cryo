import csv
import shutil
import random
import string
from datetime import datetime, timedelta
from tkinter import Label, Frame, Button, CENTER, LEFT, DISABLED, NORMAL, Tk, font, messagebox
from PIL import Image, ImageTk
from menu_common import MenuBase
from plot_generator import generate_plot


def random_generator(size=6, chars=string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


class ScreenVistaPrevia(MenuBase):
    def __init__(self, parent, controller):
        MenuBase.__init__(self, parent, controller)
        self.controller = controller
        self.make_widgets()
        self.all_focuses = [self.bt_save]
        self.seeding = False

    def save_curva_temporal(self):
        # Hay que hacer una ventana que te deje armar el nombre, ahora es random
        nombre = random_generator()
        shutil.copyfile('/tmp/cryo/curva_temporal.csv', 'curvas/ovulo/{}.csv'.format(nombre))
        self.controller.show_principal()

    def make_buttons(self):
        self.bt_save = Button(self, highlightcolor="red", highlightthickness=4, text='Save', command=lambda: self.save_curva_temporal(), width=13, font=self.fuente_button)
        self.bt_save.grid(row=1, column=0)

    def make_label_graph(self, update=False):
        if update:
            image = Image.open('/tmp/cryo/curva_temporal.png')
            photo = ImageTk.PhotoImage(image, master=self)
            self.label_img = Label(self, image=photo)
            self.label_img.image = photo
        else:
            self.label_img = Label(self)
        self.label_img.grid(row=0, column=0)

    def make_widgets(self):
        self.make_buttons()
        self.make_label_graph()
        #self.grid_columnconfigure(1, minsize=100)
        #self.grid_rowconfigure(5, minsize=60)
        #self.grid_rowconfigure(2, minsize=20)
        #self.grid_rowconfigure(1, minsize=20)

    def postupdate(self):
        self.bt_save.focus_set()
        self.actual_focus = 0
        self.bind_class("Button", "<Down>", lambda event: self.move_focus(+1))
        self.bind_class("Button", "<Up>", lambda event: self.move_focus(-1))

        self.make_label_graph(True)
