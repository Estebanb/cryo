from tkinter import Frame, Button, LEFT, DISABLED, NORMAL, Tk


def focus_next_window(event):
    event.widget.tk_focusNext().focus()


def enter(nada):
    print("esto he enter")


class Hello(Frame):                     
    def __init__(self, parent=None):    
        Frame.__init__(self, parent)
        self.pack()
        self.make_widgets()

    def make_widgets(self):             
        bt1 = Button(self, text='Hello world', command=self.quit)
        bt2 = Button(self, text='Hello world', command=self.quit)
        bt1.bind('<Return>', enter)
        bt1.pack(side=LEFT)
        bt2.pack(side=LEFT)
        bt1.configure(state=DISABLED, background='cadetblue')
        bt1.configure(state=NORMAL, background='red')
        bt1.focus_set()
        bt1.bind("<Right>", focus_next_window)
        bt2.bind("<Right>", focus_next_window)
        bt1.bind("<Left>", focus_next_window)
        bt2.bind("<Left>", focus_next_window)


if __name__ == '__main__':
    root = Tk()
    root.geometry("320x240")
    # root.attributes("-fullscreen", True)
    app = Hello(root)
    root.mainloop()

