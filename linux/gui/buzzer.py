import Adafruit_BBIO.GPIO as GPIO


class Buzzer():
    def __init__(self, pin="P2_24"):
        self.pin = pin

    def configure(self):
        GPIO.setup(self.pin, GPIO.OUT)

    def toggle(self):
        if GPIO.input(self.pin):
            GPIO.output(self.pin, GPIO.LOW)
        else:
            GPIO.output(self.pin, GPIO.HIGH)

    def turn_off(self):
        GPIO.output(self.pin, GPIO.LOW)

    def turn_on(self):
        GPIO.output(self.pin, GPIO.HIGH)
