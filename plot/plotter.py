import argparse
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser()
parser.add_argument("report")
parser.add_argument("--graph_name", default='Unnamed Graph')

args = parser.parse_args()
print(args.report)

temperaturas_medidas = []
temperaturas_deseadas = []
control_error = []
dutys = []

with open(args.report) as report:
    for line in report:
        temp_actual, output, duty, bridge_side, temp_to_go=line.split(',')
        temperaturas_medidas.append(float(temp_actual))
        temperaturas_deseadas.append(float(temp_to_go))
        control_error.append(abs(float(temp_to_go) - float(temp_actual)))
        if bridge_side == "A":
            dutys.append(0)
        else:
            dutys.append(float(duty)/10)


plt.ylabel('Temperature(in celcius)')
plt.title(args.graph_name)
plt.xlabel('Time(in seconds)')
print("Cantidad de muestras", len(temperaturas_medidas))
plt.plot(temperaturas_medidas[:-3], label='Temperatura Adquirida')
plt.plot(temperaturas_deseadas[:-3], label='Temperatura Buscada')
plt.plot(control_error[:-3], label='Control Error')
plt.plot(dutys[:-3], label='Duty Cycle')
plt.legend()
plt.annotate('Max Control Error: {} ºC'.format(round(max(control_error[1000:]), 2)), xy=(control_error.index(max(control_error[1000:])), max(control_error[1000:])), xytext=(100, -20), arrowprops=dict(facecolor='black', shrink=0.03))
plt.show()

