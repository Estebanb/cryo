import Adafruit_BBIO.PWM as PWM
import Adafruit_BBIO.GPIO as GPIO
import logging

logging.basicConfig(level=logging.DEBUG)

class HBridge():
    def __init__(self, INA="P2_5", INB="P2_3", PWM="P2_1", FREQ=500):
        self.INA = INA
        self.INB = INB
        self.PWM = PWM
        self.FREQ = FREQ
        self._setup_gpios()

    def _setup_gpios(self):  
        GPIO.setup(self.INA, GPIO.OUT)
        GPIO.setup(self.INB, GPIO.OUT)
        PWM.start(self.PWM, 0, self.FREQ)

    def turn_off_bridge(self):
        GPIO.output(self.INA, GPIO.LOW)
        GPIO.output(self.INB, GPIO.LOW)
        PWM.start(self.PWM, 0, self.FREQ)

    def set_bridge(self, duty, side):
        logging.debug("Set Bridge duty:{} side:{}".format(duty, side))
        if side == "A":
            GPIO.output(self.INA, GPIO.LOW)
            GPIO.output(self.INB, GPIO.HIGH)
        elif side == "B":
            GPIO.output(self.INA, GPIO.HIGH)
            GPIO.output(self.INB, GPIO.LOW)
        else:
            raise Exception("Not supported side, choices are A and B")
        PWM.start(self.PWM, duty, self.FREQ)

